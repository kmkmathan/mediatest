var app = angular.module("mediaApp",[]);

//component to handle async api request
app.factory("DataService", ['$http' , '$q', function($http, $q){
	return {
		getData : function (queryString) {
	        var deferred = $q.defer();
	        $http({
	            method: 'GET',
	            url: "http://www.omdbapi.com/?"+queryString
	        }).
	        success(function (data, status, headers, config) {
	            deferred.resolve(data);
	        }).
	        error(function (error, status, headers, config) {
	            deferred.reject(error);
	        });
	        return deferred.promise;
	    }
	};
}]);

app.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
        iElement.autocomplete({
            source: scope[iAttrs.uiItems],
            select: function() {
                $timeout(function() {
                  iElement.trigger('input');
                }, 0);
            }
        });
    };
});

app.directive('rating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '='
        },
        link: function (scope, elem, attrs) {
            scope.stars = [];
            for (var i = 0; i < scope.max; i++) {
                scope.stars.push({
                    filled: i < scope.ratingValue
                });
            }
        }
    }
});

app.filter('roundRating', function() {
    return function(input) {
		var rating = parseInt(input, 10);
		return Math.round((rating / 2));
    };
});


//component for only for search
app.controller("SearchCtrl", ["$rootScope", "$scope", "DataService", function($rootScope, $scope, DataService){
	$scope.titles = [];

	//autocomplete based on the api response
	var autoCompleteSuccessHandler = function(data){
		if(data.Response === "True" && data.Search && data.Search.length){
			data.Search.forEach(function(value){
				var title = value.Title;
				$scope.titles.push(value.Title);
			});
		}
	};

	var autocompleteFailureHanlder = function(error){
		alert("Error occurred", error);
	};

	//get movie titles based on the search keyword
	//condition minimum length should be 2 because 
	//omdb expecting minimum 2 letters
	$scope.autoComplete = function(){
		var searchKeyword = $scope.keyword;
		if(searchKeyword.length >= 2){
			var queryString = "s="+searchKeyword;
			DataService.getData(queryString).then(autoCompleteSuccessHandler, autocompleteFailureHanlder);
		}
	};

	//search movie title
	//get all imdbID releated to the keyword searched
	//we can use imdbID to fetch full information of imdbID
	//for example, actor, director, rating, etc
	$scope.search = function(keyword, event){
		if(event.which === 13){
			var queryString = "s="+keyword;
			DataService.getData(queryString)
			.then(function(data, status){
				var imdbIDs = [];
				if(data.Response === "True" && data.Search && data.Search.length){
					data.Search.forEach(function(value){
						imdbIDs.push(value.imdbID);
					});
					$rootScope.$broadcast("handleSearch", imdbIDs);
				}
			}, function(error, status){
				console.log(error);
			});
		}
	}
}]);

//component related to movies separated from search
app.controller("MovieCtrl", ["$scope", "DataService", "$q", function($scope, DataService, $q){
	var successHandler = function(data){
		$scope.movieList = data;
	};

	var failureHanlder = function(error){
		alert("Error occurred", error);
	};

	$scope.$on("handleSearch", function(event, imdbIDs){
		var promises = [];
		imdbIDs.forEach(function(imdbId){
			promises.push(DataService.getData("i="+imdbId));
		});
		$q.all(promises).then(successHandler, failureHanlder);
	});

}]);

